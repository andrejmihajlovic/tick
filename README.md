# PostgreSQL_extensible plugin
Original telegraf readme can be found in README_ORIGINAL.md. 

This postgresql plugin provides metrics for your postgres database. It has been
designed to parse SQL queries in the plugin section of your `telegraf.conf`.


To add new queries edit files: 

* config/telegraf_pg_queries_database_scope.conf - used to define queries that are database specific - queries that depend on objects and data contained within a specific database(s). eg. "SELECT ... FROM information_schema.tables"
* config/telegraf_pg_queries_server_scope.conf - used to define queries that are database agnostic - queries that depend only on cluster objects. eg. "SELECT ... FROM pg_stat_database"

Define the toml config where the sql queries are stored
Structure :

```
[[inputs.postgresql_extensible.query]]
   sqlquery string -> [mandatory] Sql query. Multi line strings should begin and end with triple double quote. Escape all dublequotes in the query.
   version string -> [optional] Minimal postgresql version required to run the query. 
   tagvalue string -> [optional] Comma separated list of tags in the query result set.  (Hint: Tags are grouping columns. eg. host_name, database_name, schema_name, table_name...)
   timestamp string -> [optional] Timestamp column name. Used if query contains a temporal column that should be used to sort and query data. 
   measurement string -> [mandatory] Measurement name. Measurement is more or less equivalent to standard SQL table object. 
   collectioninterval interval -> [optional] How often should the query be run. Interval begins at service restart. For most non mission critical values this should be X minutes/hours. 
```

Example:

```
[[inputs.postgresql_extensible.query]]
  sqlquery="""
		select
            _queued_at as \"timestamp\",
            case when status  IN('OK', 'WARN') THEN 1 ELSE 0 END \"isUp\",
            status, \"availableMemory\",
            \"activeMasterQueries\",
            \"activeSlaveQueries\",
            \"smdVersion\",
            \"activeTemplater\",
            \"activeRequests\",
            \"schedulerRunning\",
            \"migrationImport\"
        from
            health.\"HealthCheckLog\"
        where
            _queued_at >= now() - interval '10 minutes'"""
  tagvalue=""
  timestamp="timestamp"
  measurement="instafinUptime"
  collectioninterval="1m"
```

