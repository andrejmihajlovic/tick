#!/usr/bin/env python
import psycopg2
import socket
import sys

def main(confFilePath):


    templateConfig = """
[[inputs.postgresql_extensible]]
  address = "host=/var/run/postgresql/.s.PGSQL.5432 user=telegraf dbname=%database%"
  #address = "host=localhost user=telegraf dbname=%database%"
  outputaddress = \"""" + str(socket.gethostname()) + """\"
 
%queryConfigs%
"""

    basicConfig = open("telegraf_pg_basic_config.conf", "r").read().replace("\t", "    ")
    serverScopeQueries = open("telegraf_pg_queries_server_scope.conf", "r").read().replace("\t", "    ")
    databaseScopeQueries = open("telegraf_pg_queries_database_scope.conf", "r").read().replace("\t", "    ")
    
    finalConfig = basicConfig  + """		
###############################################################################
#                            INPUT PLUGINS                                    #
###############################################################################

##### SERVER SCOPE QUERIES #####""" + templateConfig.replace("%database%", "postgres").replace("%queryConfigs%", serverScopeQueries)
    
    
    conn = psycopg2.connect("dbname=postgres")
    curs = conn.cursor()
    curs.execute("select datname from pg_database where datname like 'instafin%';")
    databases = curs.fetchall()
    conn.close()
    
    for database in databases:
        finalConfig = finalConfig + "\n##### QUERIES FOR DATABASE: " + database[0] + " ##### \n" + templateConfig.replace("%database%", database[0]).replace("%queryConfigs%", databaseScopeQueries)
    
    finalConfigFile = open(confFilePath, "w")
    finalConfigFile.truncate()
    finalConfigFile.write(finalConfig.strip())
    finalConfigFile.close()


confFilePath = "telegraf_pg.conf"
if len(sys.argv) > 1:
	confFilePath = sys.argv[1]	
main(confFilePath)