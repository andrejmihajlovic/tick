#! /usr/bin/env bash

if [[ "$1" = "t" ]]
then 
	cd /opt/tick/config
	python generate_telegraf_config.py "/etc/telegraf_pg/telegraf_pg.conf"
	service telegraf_pg restart
else 
	bash -c '
	cd /opt/tick/
	git reset --hard
	git fetch
	git pull --dry-run | grep -q -v "Already up-to-date." && changed=1
	if [[ $changed = 1 ]]
	then
		git pull
		cd config
		python generate_telegraf_config.py "/etc/telegraf_pg/telegraf_pg.conf"
		service telegraf_pg restart
	else 
		echo "No change detected"
	fi
	'
fi