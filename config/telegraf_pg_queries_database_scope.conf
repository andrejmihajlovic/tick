[[inputs.postgresql_extensible.query]]
  #Instafin uptime
  sqlquery="""
SELECT
    _queued_at AS \"timestamp\"
    , CASE
        WHEN status IN('OK', 'WARN', 'CRITICAL') THEN 1
            ELSE 0
    END isUp
    , status
    , \"availableMemory\"
    , \"activeMasterQueries\"
    , \"activeSlaveQueries\"
    , \"smdVersion\"
    , \"activeTemplater\"
    , \"activeRequests\"
    , \"schedulerRunning\"
    , \"migrationImport\"
    , EXTRACT(EPOCH FROM (
        SELECT
            MIN(\"_processed_at\")
        FROM
            health.\"HealthCheckLog\"
    ))::int AS \"minTS\"
FROM
    health.\"HealthCheckLog\"
WHERE
    _queued_at >= now() - interval '5 minutes'
	"""
  withdbname=false
  tagvalue="status,minTS"
  timestamp="timestamp"
  measurement="instafinHealthCheckLog"
  collectioninterval="10s"

[[inputs.postgresql_extensible.query]]
  #Index stats
  sqlquery="""
SELECT
    table_schema
    , table_name
    , index_name
    , real_size_mb
    , extra_mb
    , extra_ratio
    , fillfactor
    , bloat_mb
    , bloat_ratio
    , idx_blks_read
    , idx_blks_hit
    , idx_scan
    , idx_tup_read
    , idx_tup_fetch
FROM
    (
        SELECT
            nspname                                             AS table_schema
            , tblname                                           AS table_name
            , idxname                                           AS index_name
            , ROUND((bs *(relpages) / (1024 * 1024)), 2)::float AS real_size_mb
            , CASE
                WHEN relpages-est_pages < 0 THEN 0
                    ELSE ROUND((bs *(relpages-est_pages) / (1024 * 1024))::numeric, 2)
            END::float AS extra_mb
            , CASE
                WHEN relpages-est_pages < 0 THEN 0
                    ELSE ROUND((100 * (relpages-est_pages) / relpages )::numeric, 2)
            END::float AS extra_ratio
            , fillfactor
            , CASE
                WHEN relpages-est_pages_ff < 0 THEN 0
                    ELSE ROUND((bs *(relpages-est_pages_ff) / (1024 * 1024))::numeric ,2)
            END::float AS bloat_mb
            , CASE
                WHEN relpages-est_pages_ff < 0 THEN 0
                    ELSE ROUND((100 * (relpages-est_pages_ff) / relpages)::numeric,2)
            END::float AS bloat_ratio
            , indexrelid
        
        FROM
            (
                SELECT
                    COALESCE(1   + ceil(reltuples/floor((bs-pageopqdata-pagehdr)/(4+nulldatahdrwidth)::float)), 0 )                  AS est_pages
                    , COALESCE(1 + ceil(reltuples/floor((bs-pageopqdata-pagehdr)*fillfactor/(100*(4+nulldatahdrwidth)::float))), 0 ) AS est_pages_ff
                    , bs
                    , nspname
                    , table_oid
                    , tblname
                    , idxname
                    , relpages
                    , fillfactor
                    , is_na
                    , indexrelid
                FROM
                    (
                        SELECT
                            maxalign
                            , bs
                            , nspname
                            , tblname
                            , idxname
                            , reltuples
                            , relpages
                            , relam
                            , table_oid
                            , fillfactor
                            , ( index_tuple_hdr_bm + maxalign -
                            CASE
                                WHEN index_tuple_hdr_bm%maxalign = 0 THEN maxalign
                                    ELSE index_tuple_hdr_bm%maxalign
                            END + nulldatawidth + maxalign -
                            CASE
                                WHEN nulldatawidth                   = 0 THEN 0
                                WHEN nulldatawidth::integer%maxalign = 0 THEN maxalign
                                    ELSE nulldatawidth::integer%maxalign
                            END )::numeric AS nulldatahdrwidth
                            , pagehdr
                            , pageopqdata
                            , is_na
                            , indexrelid
                        
                        FROM
                            (
                                SELECT
                                    i.indexrelid
                                    , i.nspname
                                    , i.tblname
                                    , i.idxname
                                    , i.reltuples
                                    , i.relpages
                                    , i.relam
                                    , a.attrelid                             AS table_oid
                                    , current_setting('block_size')::numeric AS bs
                                    , fillfactor
                                    , CASE
                                        WHEN version() ~ 'mingw32'
                                            OR version() ~ '64-bit|x86_64|ppc64|ia64|amd64' THEN 8
                                            ELSE 4
                                    END  AS maxalign
                                    , 24 AS pagehdr
                                    , 16 AS pageopqdata
                                    , CASE
                                        WHEN MAX(COALESCE(s.null_frac,0)) = 0 THEN 2
                                            ELSE 2 + (( 32 + 8 - 1 ) / 8)
                                    END                                                                AS index_tuple_hdr_bm
                                    , SUM( (1-COALESCE(s.null_frac, 0)) * COALESCE(s.avg_width, 1024)) AS nulldatawidth
                                    , MAX(
                                        CASE
                                            WHEN a.atttypid = 'pg_catalog.name'::regtype THEN 1
                                                ELSE 0
                                        END
                                    ) > 0 AS is_na
                                FROM
                                    pg_attribute AS a
                                    JOIN
                                        (
                                            SELECT
                                                nspname
                                                , tbl.relname AS tblname
                                                , idx.relname AS idxname
                                                , idx.reltuples
                                                , idx.relpages
                                                , idx.relam
                                                , indrelid
                                                , indexrelid
                                                , indkey::smallint[]                                                                                  AS attnum
                                                , COALESCE(substring( array_to_string(idx.reloptions, ' ') FROM 'fillfactor=([0-9]+)')::smallint, 90) AS fillfactor
                                            FROM
                                                pg_index
                                                JOIN pg_class idx
                                                    ON  idx.oid=pg_index.indexrelid
                                                JOIN pg_class tbl
                                                    ON  tbl.oid=pg_index.indrelid
                                                JOIN pg_namespace
                                                    ON  pg_namespace.oid = idx.relnamespace
                                            WHERE
                                                pg_index.indisvalid
                                                AND tbl.relkind  = 'r'
                                                AND idx.relpages > 0
                                        )
                                        AS i
                                        ON  a.attrelid = i.indexrelid
                                    LEFT JOIN pg_stats AS s
                                        ON  s.schemaname = i.nspname
                                            AND
                                            (
                                                (
                                                    s.tablename   = i.tblname
                                                    AND s.attname = pg_catalog.pg_get_indexdef(a.attrelid, a.attnum, TRUE)
                                                )
                                                OR
                                                (
                                                    s.tablename   = i.idxname
                                                    AND s.attname = a.attname
                                                )
                                            )
                                    JOIN pg_type AS t
                                        ON  a.atttypid = t.oid
                                WHERE
                                    a.attnum > 0
                                GROUP BY
                                    1
                                    , 2
                                    , 3
                                    , 4
                                    , 5
                                    , 6
                                    , 7
                                    , 8
                                    , 9
                                    , 10
                            )
                            AS s1
                    )
                               AS s2
                    JOIN pg_am    am
                        ON  s2.relam = am.oid
                WHERE
                    am.amname = 'btree'
            )
            AS sub
    )
                               s
    JOIN pg_statio_all_indexes sai
        ON  s.indexrelid = sai.indexrelid
    JOIN pg_stat_all_indexes sai1
        ON  s.indexrelid = sai1.indexrelid

WHERE
    table_schema NOT IN ('pg_catalog', 'information_schema')
        """
  withdbname=false
  tagvalue="table_schema,table_name,index_name"
  measurement="indexStats"
  collectioninterval="4h"



  [[inputs.postgresql_extensible.query]]
  #Table stats
  sqlquery="""
       WITH constants AS
    (
        SELECT
            current_setting('block_size')::numeric AS bs
            , 23                                   AS hdr
            , 8                                    AS ma
    )
    , base_stats AS
    (
        SELECT
          c.oid     relid
            , n.nspname table_schema
            , c.relname table_name
            , stat.n_live_tup::numeric                     AS est_rows
            , stat.seq_scan
            , stat.seq_tup_read
            , stat.idx_scan
            , stat.idx_tup_fetch
            , stat.n_tup_ins
            , stat.n_tup_del
            , stat.n_tup_upd
            , stat.n_tup_hot_upd
            , stat.n_live_tup
            , stat.n_dead_tup
            , stat.n_mod_since_analyze
            , stat.last_vacuum
            , stat.last_autovacuum
            , stat.last_analyze
            , stat.last_autoanalyze
            , stat.vacuum_count
            , stat.autovacuum_count
            , stat.analyze_count
            , stat.autoanalyze_count
            , pg_table_size(relid)::numeric           AS data_bytes
            , pg_table_size(relid)::numeric - pg_total_relation_size(c.reltoastrelid) AS table_bytes
            , pg_indexes_size(c.oid)                  AS index_bytes
            , pg_total_relation_size(c.reltoastrelid) AS toast_bytes
            , c.reltuples
            , c.reltoastrelid
            , CASE WHEN attname IS NULL THEN FALSE ELSE TRUE END bloat_can_be_analyzed
            , hdr+1+(SUM(
                CASE
                    WHEN null_frac <> 0 THEN 1
                        ELSE 0
                END
            )/8)                           AS nullhdr
            , SUM((1-null_frac)*avg_width) AS datawidth
            , MAX(null_frac)               AS maxfracsum
            , hdr
            , ma
            , bs
        FROM
            pg_class c
            cross join constants
            JOIN pg_namespace n
        ON c.relnamespace = n.oid
            JOIN pg_stat_user_tables AS stat
                ON  stat.relid = c.oid

            LEFT JOIN pg_stats ps
                ON n.nspname = ps.schemaname
                    AND c.relname = ps.tablename
                           WHERE
            n.nspname NOT IN ('pg_catalog', 'information_schema')
        group by
          hdr
            , ma
            , bs
        , c.oid
            , n.nspname
            , c.relname
            , c.reltoastrelid
            , stat.n_live_tup::numeric
            , c.reltuples
            , stat.seq_scan
            , stat.seq_tup_read
            , stat.idx_scan
            , stat.idx_tup_fetch
            , stat.n_tup_ins
            , stat.autovacuum_count
            , stat.n_tup_del
            , stat.n_tup_upd
            , stat.n_tup_hot_upd
            , stat.n_live_tup
            , stat.n_dead_tup
            , stat.n_mod_since_analyze
            , stat.last_vacuum
            , stat.last_autovacuum
            , stat.last_analyze
            , stat.last_autoanalyze
            , stat.vacuum_count
            , stat.analyze_count
            , stat.autoanalyze_count
            , pg_table_size(relid)::numeric
            , pg_indexes_size(c.oid)
            , pg_total_relation_size(c.reltoastrelid)
            , CASE WHEN attname IS NULL THEN FALSE ELSE TRUE END
    )
    , data_headers AS
    (
        SELECT
            base_stats.*
            , (datawidth+(hdr+ma-(
                CASE
                    WHEN hdr%ma=0 THEN ma
                        ELSE hdr%ma
                END
            )))::numeric AS datahdr
            , (maxfracsum*(nullhdr+ma-(
                CASE
                    WHEN nullhdr%ma=0 THEN ma
                        ELSE nullhdr%ma
                END
            ))) AS nullhdr2
        FROM
            base_stats
    )
    , table_estimates AS
    (
        SELECT
             dh.*
            , (CEIL((dh.reltuples* (datahdr + nullhdr2 + 4 + ma - (
                CASE
                    WHEN datahdr%ma=0 THEN ma
                        ELSE datahdr%ma
                END
            ) )/(bs-20))) * bs )::NUMERIC AS expected_table_bytes

              , (( ceil( COALESCE(toast.reltuples, 0) / 4 ) * bs ) )::NUMERIC AS expected_toast_bytes
        FROM
        data_headers dh
            LEFT OUTER JOIN pg_class AS toast
                ON  dh.reltoastrelid = toast.oid
                    AND toast.relkind             = 't'
    )
    , table_estimates_plus AS
    (
        SELECT
            CASE
                WHEN expected_table_bytes + expected_toast_bytes    > 0
                    AND table_bytes     > 0
                    AND expected_table_bytes + expected_toast_bytes <= table_bytes + toast_bytes THEN ((table_bytes + toast_bytes) - (expected_table_bytes + expected_toast_bytes))::NUMERIC
                    ELSE 0::NUMERIC
            END AS bloat_bytes
            ,*
        FROM
            table_estimates

    )
        SELECT
              table_schema
            , table_name
            , COALESCE(ROUND(bloat_bytes   *100/NULLIF(table_bytes,0)), 0)::float     AS pct_bloat
            , COALESCE(ROUND(bloat_bytes   /(1024^2)::NUMERIC,2), 0)::float AS bloat_mb
            , COALESCE(ROUND(data_bytes/(1024^2)::NUMERIC,3), 0)::float AS data_mb
            , COALESCE(ROUND(table_bytes/(1024^2)::NUMERIC,3), 0)::float AS table_mb
            , COALESCE(ROUND(toast_bytes/(1024^2)::NUMERIC,3), 0)::float AS toast_mb
            , COALESCE(ROUND(index_bytes/(1024^2)::NUMERIC,3), 0)::float AS index_mb
            , COALESCE(ROUND(expected_table_bytes/(1024^2)::NUMERIC,3), 0)::float AS expected_table_mb
            , COALESCE(ROUND(expected_toast_bytes/(1024^2)::NUMERIC,3), 0)::float AS expected_toast_mb
            , analyze_count
            , autoanalyze_count
            , bloat_bytes::bigint as bloat_bytes
            , bloat_can_be_analyzed 
            , data_bytes::bigint as data_bytes
            , expected_table_bytes::bigint as expected_table_bytes
            , expected_toast_bytes::bigint as expected_toast_bytes
            , idx_scan
            , idx_tup_fetch
            , index_bytes
            , maxfracsum
            , n_dead_tup
            , n_live_tup           
            , n_mod_since_analyze   
            , n_tup_del
            , n_tup_hot_upd
            , n_tup_ins
            , n_tup_upd
            , tep.relid
            , reltoastrelid
            , reltuples
            , seq_scan
            , seq_tup_read
            , table_bytes
            , toast_bytes
            , vacuum_count
            , autovacuum_count
            , sut.heap_blks_read
            , sut.heap_blks_hit
            , sut.idx_blks_read
            , sut.idx_blks_hit
            , sut.toast_blks_read
            , sut.toast_blks_hit
            , sut.tidx_blks_read
            , sut.tidx_blks_hit
        FROM
            table_estimates_plus tep
            left join pg_statio_all_tables sut
                on tep.relid = sut.relid
        
    
        
        """
  withdbname=false
  tagvalue="table_schema,table_name"
  measurement="tableStats"
  collectioninterval="4h"

