#preq for installing and reloading config
yum -y install git

#setup user acc
useradd -r telegraf

#change open file limit 
ulimit -n 65536

#prepare folders
mkdir -p /opt/tick
mkdir -p /etc/telegraf_pg
mkdir -p /var/log/telegraf
mkdir -p /var/run/telegraf

#clone repo
cd /opt
git clone https://:@bitbucket.org/andrejmihajlovic/tick.git 

#setup folder ownership
chown -R telegraf /opt/tick
chown -R telegraf /var/log/telegraf
chown -R telegraf /var/run/telegraf
chown -R telegraf /etc/telegraf_pg

#copy telegraf binary to /user/bin/telegraf_pg
#scp user@host:/path/to/telegraf /usr/bin/telegraf_pg
#or build repo locally if golang compiler is installed

chown telegraf /usr/bin/telegraf_pg
chmod +x /usr/bin/telegraf_pg

#setup service script
cp /opt/tick/config/service /etc/init.d/telegraf_pg
chown telegraf /etc/init.d/telegraf_pg
chmod +x /etc/init.d/telegraf_pg
####open /etc/init.d/telegraf_pg and change influx host, username and password or add them as env vars


#### On cluster master server run:
#su postgres -s /bin/bash -c 'psql -c "CREATE ROLE telegraf WITH LOGIN SUPERUSER;" '


#Setup postgresql telegraf user
#Depending on the environment run either stg or prod version
#echo "local    all    telegraf        peer" >> /etc/postgresql/stg/pg_hba.conf
#echo "local    all    telegraf        peer" >> /etc/postgresql/prod/pg_hba.conf

su postgres -s /bin/bash -c 'psql -c "select pg_reload_conf();" '


#Generate config and start service
chmod +x /opt/tick/config/reload_telegraf_pg.sh
su telegraf -s /bin/bash -c "/opt/tick/config/reload_telegraf_pg.sh"

#Add reload to crontab to automate query changes
(crontab -u telegraf -l; echo "0 19 * * * /opt/tick/config/reload_telegraf_pg.sh" ) | crontab -u telegraf -

